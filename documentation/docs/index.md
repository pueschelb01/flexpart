# Welcome to the FLEXPART 11 documentation

- Information on how to download and install FLEXPART can be found [here](building.md).
- How to set up a simulation, with an explanation of all input files can be found [here](configuration.md#config), and some examples can be found [here](examples.md).
- A list of all possible output options and files can be found [here](output.md).
- An overview of all processes relation to the direct transport of particles can be found [here](transport.md), and internal particle processes [here](evolution.md).

This manual covers FLEXPART 11. A full description can be found in the official release paper:

However, this manual might proof useful for earlier versions as well. Please cite the correct version when using an older version:

- [FLEXPART 10.4](https://gmd.copernicus.org/articles/12/4955/2019/)
- [FLEXPART 9]()
- [FLEXPART 6.2](https://acp.copernicus.org/articles/5/2461/2005/)